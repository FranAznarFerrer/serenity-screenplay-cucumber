package com.everis.automationframework.app.questions;

import com.everis.automationframework.app.model.ClientData;
import net.serenitybdd.screenplay.Question;

public class Login {

    public static Question<Boolean> isFailed(String errorMessage) {
       return LoginFailed.isErrorShowing(errorMessage);
    }
    public static Question<Boolean> isLogged(ClientData clientData) {
        return LoginSuccess.isLogged(clientData);
    }

}
