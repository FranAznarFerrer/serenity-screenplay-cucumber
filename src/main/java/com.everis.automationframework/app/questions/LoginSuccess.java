package com.everis.automationframework.app.questions;

import com.everis.automationframework.app.model.ClientData;
import lombok.AllArgsConstructor;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.everis.automationframework.app.ui.ECommercePage.ACCOUNT;

@AllArgsConstructor
public class LoginSuccess implements Question<Boolean> {

    private ClientData clientData;

    @Override
    public Boolean answeredBy(Actor actor) {
        return ACCOUNT.resolveFor(actor).getText().equals(clientData.getFirstName() + " " + clientData.getLastName());
    }

    public static Question<Boolean> isLogged(ClientData clientData) {
        return new LoginSuccess(clientData);
    }

}
