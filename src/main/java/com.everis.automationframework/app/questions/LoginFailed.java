package com.everis.automationframework.app.questions;

import lombok.AllArgsConstructor;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.everis.automationframework.app.ui.AuthenticationPage.ERROR_MESSAGE;

@AllArgsConstructor
public class LoginFailed implements Question<Boolean> {

    private String errorMessage;

    @Override
    public Boolean answeredBy(Actor actor) {
        return ERROR_MESSAGE.resolveFor(actor).isPresent() &&
                ERROR_MESSAGE.resolveFor(actor).getText().equals(errorMessage);
    }

    public static Question<Boolean> isErrorShowing(String errorMessage) {
        return new LoginFailed(errorMessage);
    }

}
