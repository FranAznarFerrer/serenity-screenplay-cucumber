package com.everis.automationframework.app.interaction;

import com.everis.automationframework.app.model.enums.clientdata.Title;
import com.everis.automationframework.app.ui.CreateAccountPage;
import lombok.AllArgsConstructor;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;

import java.util.Arrays;
import java.util.List;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;

@AllArgsConstructor
public class ChooseATitle implements Interaction {

    private Title title;

    @Override
    public <T extends Actor> void performAs(T actor) {
        Target selectedTitle = getTitleById().get(title.getId() - 1);
        actor.attemptsTo(WaitUntil.the(selectedTitle, isCurrentlyEnabled()).forNoMoreThan(10).seconds());
        actor.attemptsTo(Click.on(selectedTitle));
    }

    private static List<Target> getTitleById() {
        return Arrays.asList(CreateAccountPage.TITLE_MR, CreateAccountPage.TITLE_MRS);
    }

    public static ChooseATitle choose(Title title) {
        return instrumented(ChooseATitle.class, title);
    }


}
