package com.everis.automationframework.app.model;

import com.everis.automationframework.app.model.enums.clientdata.Country;
import com.everis.automationframework.app.model.enums.clientdata.State;
import com.everis.automationframework.app.model.enums.clientdata.Title;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import static lombok.AccessLevel.PUBLIC;

@ToString
@AllArgsConstructor
@Getter(PUBLIC)
public class ClientData {

    private Title title;
    private String firstName;
    private String lastName;
    private Credentials credentials;
    private BirthDate birthDate;
    private String address;
    private String city;
    private State state;
    private String postalCode;
    private Country country;
    private String phoneNumber;

}
