package com.everis.automationframework.app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import static lombok.AccessLevel.PUBLIC;

@ToString
@AllArgsConstructor
@Getter(PUBLIC)
public class Credentials {

    private String email;
    private String password;

}
