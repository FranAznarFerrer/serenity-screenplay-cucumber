package com.everis.automationframework.app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import static lombok.AccessLevel.PUBLIC;

@ToString
@Getter(PUBLIC)
@AllArgsConstructor
public class BirthDate {

    private String birthDay;
    private String birthMonth;
    private String birthYear;

}
