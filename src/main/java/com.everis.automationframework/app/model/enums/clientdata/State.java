package com.everis.automationframework.app.model.enums.clientdata;

import lombok.Getter;

import static lombok.AccessLevel.PUBLIC;

@Getter(PUBLIC)
public enum State {

    Alabama("Alabama", 1),
    Alaska("Alaska", 2),
    Florida("Florida", 9),
    Nebraska("Nebraska", 27);

    private String name;
    private Integer id;

    State(String name, Integer id) {
        this.name = name;
        this.id = id;
    }


}
