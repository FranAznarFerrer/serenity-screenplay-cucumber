package com.everis.automationframework.app.model.enums.clientdata;

import lombok.Getter;

import static lombok.AccessLevel.PUBLIC;

@Getter(PUBLIC)
public enum Country {

    USA("United States", 21);

    private String name;
    private Integer id;

    Country(String name, Integer id) {
        this.name = name;
        this.id = id;
    }
}
