package com.everis.automationframework.app.model.enums.clientdata;

import lombok.Getter;

@Getter
public enum Title {

    Mr("Mr", 1),
    Mrs("Mrs", 2);

    private Integer id;
    private String description;

    Title(String description, Integer id) {
        this.description = description;
        this.id = id;
    }
}
