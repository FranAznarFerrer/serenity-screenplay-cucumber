package com.everis.automationframework.app.model.enums;

import lombok.Getter;

import static lombok.AccessLevel.PUBLIC;

@Getter(PUBLIC)
public enum ErrorMessage {

    AUTHENTICATION_FAILED("Authentication failed.");

    private String messageDescription;

    ErrorMessage(String messageDescription) {
        this.messageDescription = messageDescription;
    }
}
