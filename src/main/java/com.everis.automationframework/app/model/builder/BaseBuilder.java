package com.everis.automationframework.app.model.builder;

import lombok.AllArgsConstructor;
import net.serenitybdd.screenplay.Actor;

@AllArgsConstructor
public class BaseBuilder {

    protected Actor actor;

}
