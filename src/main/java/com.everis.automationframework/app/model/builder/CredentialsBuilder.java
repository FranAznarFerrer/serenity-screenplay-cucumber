package com.everis.automationframework.app.model.builder;

import com.everis.automationframework.app.model.Credentials;
import net.serenitybdd.screenplay.Actor;

import static com.everis.automationframework.app.model.builder.utils.BuilderUtils.generateRandomString;

public class CredentialsBuilder extends BaseBuilder {

    public CredentialsBuilder(Actor actor) {
        super(actor);
    }

    public Credentials build() {
        return new Credentials(
                generateEmail(actor.getName()),
                generatePassword()
        );
    }

    private String generateEmail(String actorName) {
        return actorName + "_" + generateRandomString(4) + "@demo.com";
    }

    private String generatePassword() {
        return generateRandomString(6);
    }


}
