package com.everis.automationframework.app.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Step;

import static com.everis.automationframework.app.ui.ECommercePage.ACCOUNT;
import static com.everis.automationframework.app.ui.ECommercePage.SIGN_OUT;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isNotVisible;

public class Logout implements Task {

    @Override
    @Step("{0} logout in the ecommerce")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(SIGN_OUT));
        actor.attemptsTo(WaitUntil.the(ACCOUNT, isNotVisible()).forNoMoreThan(5).seconds());
    }

    public static Logout closeSession() {
        return instrumented(Logout.class);
    }


}

