package com.everis.automationframework.app.task;


import com.everis.automationframework.app.interaction.CreateAccountForm;
import com.everis.automationframework.app.interaction.SigInForm;
import com.everis.automationframework.app.model.ClientData;
import lombok.AllArgsConstructor;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Step;

import static com.everis.automationframework.app.ui.CreateAccountPage.MOBILE_PHONE;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isNotCurrentlyEnabled;

@AllArgsConstructor
public class SignIn implements Task {

    private final ClientData clientData;

    @Override
    @Step("{0} creates a new account")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(CreateAccountForm.fillWith(clientData.getCredentials().getEmail()));
        actor.attemptsTo(SigInForm.fillWith(clientData));
        actor.attemptsTo(WaitUntil.the(MOBILE_PHONE, isNotCurrentlyEnabled()));
    }

    public static SignIn createNewAccountAs(ClientData clientData) {
        return instrumented(SignIn.class, clientData);
    }

}
