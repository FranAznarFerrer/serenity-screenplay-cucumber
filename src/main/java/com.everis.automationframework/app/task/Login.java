package com.everis.automationframework.app.task;

import com.everis.automationframework.app.interaction.LogInForm;
import com.everis.automationframework.app.model.Credentials;
import lombok.AllArgsConstructor;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

@AllArgsConstructor
public class Login implements Task {

    private Credentials credentials;

    @Override
    @Step("{0} login in the ecommerce")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(LogInForm.fillWith(credentials));
    }

    public static Login with(Credentials credentials) {
        return instrumented(Login.class, credentials);
    }

}
