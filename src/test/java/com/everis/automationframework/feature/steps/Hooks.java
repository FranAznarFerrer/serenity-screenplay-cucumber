package com.everis.automationframework.feature.steps;

import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.webdriver.WebdriverManager;
import org.junit.BeforeClass;

public class Hooks {

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

}
