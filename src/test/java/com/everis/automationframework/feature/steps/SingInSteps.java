package com.everis.automationframework.feature.steps;

import com.everis.automationframework.app.model.ClientData;
import com.everis.automationframework.app.model.Credentials;
import com.everis.automationframework.app.model.builder.ClientBuilder;
import com.everis.automationframework.app.model.builder.CredentialsBuilder;
import com.everis.automationframework.app.model.enums.ErrorMessage;
import com.everis.automationframework.app.task.GoTo;
import com.everis.automationframework.app.task.Login;
import com.everis.automationframework.app.task.NavigateTo;
import com.everis.automationframework.app.task.SignIn;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static com.everis.automationframework.app.task.Logout.closeSession;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

public class SingInSteps {


    private Credentials credentials;
    private ClientData clientData;

    @Given("^that (Fran|Saul|Rafa) is already registered in the ecommerce$")
    public void theClientIsRegisteredAtThePlatform(String actorName) {
        credentials = new CredentialsBuilder(theActorCalled(actorName)).build();
        clientData = new ClientBuilder(theActorInTheSpotlight(), credentials).build();

        theActorInTheSpotlight().wasAbleTo(
                GoTo.eCommerce(),
                NavigateTo.SignInSection(),
                SignIn.createNewAccountAs(clientData)
        );
    }

    @But("^he is not yet logged in the ecommerce$")
    public void heIsNotLoggedInTheEcommerce() {
        if (com.everis.automationframework.app.questions.Login.isLogged(clientData).answeredBy(theActorInTheSpotlight())) {
            theActorInTheSpotlight().attemptsTo(closeSession());
        }
    }

    @Given("^that (Fran|Saul|Rafa) isn't registered in the ecommerce$")
    public void theClientIsNotRegisteredAtThePlatform(String actorName) {
        credentials = new CredentialsBuilder(theActorCalled(actorName)).build();
        theActorInTheSpotlight().attemptsTo(
                GoTo.eCommerce());
    }

    @When("^(?:he|she) login with your credentials in the login page$")
    public void theClientFillWithYourCorrectCredentialsTheLogin() {
        theActorInTheSpotlight().attemptsTo(
                GoTo.eCommerce(),
                NavigateTo.SignInSection(),
                Login.with(credentials)
        );
    }

    @Then("^(?:he|she) should see a (AUTHENTICATION_FAILED) error message$")
    public void theClientShouldAccessToTheCalculatorPriceApp(ErrorMessage errorMessage) {
        theActorInTheSpotlight().should(
                seeThat("the login show an error message", com.everis.automationframework.app.questions.Login.isFailed(errorMessage.getMessageDescription()), equalTo(true))
        );
    }

    @Then("^(?:he|she) shouldn't see a (AUTHENTICATION_FAILED) error message$")
    public void theClientShouldNotAccessToTheCalculatorPriceApp(ErrorMessage errorMessage) {
        theActorInTheSpotlight().should(
                seeThat("the login not show an error message", com.everis.automationframework.app.questions.Login.isFailed(errorMessage.getMessageDescription()), not(true))
        );

    }


}
